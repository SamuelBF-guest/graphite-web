Source: graphite-web
Section: web
Priority: extra
Maintainer: Debian Graphite Group <team+debian-graphite-team@tracker.debian.org>
Uploaders:
 Jonas Genannt <genannt@debian.org>,
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper (>= 10),
 dh-python,
 python3-all,
Build-Depends-Indep:
 python3-cairo,
 python3-django,
 python3-django-tagging,
 python3-pyparsing,
 python3-simplejson,
 python3-tz,
 python3-urllib3,
 python3-whisper (>= 1.1.4),
 python3-cairocffi,
 python3-six,
Standards-Version: 4.0.0.4
Homepage: https://github.com/graphite-project/graphite-web/
Vcs-Git: https://salsa.debian.org/debian-graphite-team/graphite-web.git
Vcs-Browser: https://salsa.debian.org/debian-graphite-team/graphite-web

Package: graphite-web
Architecture: all
Depends:
 adduser,
 python3-cairo,
 python3-django,
 python3-django-tagging,
 python3-pyparsing,
 python3-simplejson,
 python3-tz,
 python3-urllib3,
 python3-whisper (>= 1.1.4),
 python3-cairocffi,
 python3-six,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 graphite-carbon,
 libapache2-mod-wsgi-py3,
 python3-ldap,
 python3-memcache,
 python3-mysqldb,
Description: Enterprise Scalable Realtime Graphing
 Graphite consists of a storage backend and a web-based visualization
 frontend. Client applications send streams of numeric time-series
 data to the Graphite backend (called carbon), where it gets stored in
 fixed-size database files similar in design to RRD. The web frontend
 provides 2 distinct user interfaces for visualizing this data in
 graphs as well as a simple URL-based API for direct graph generation.
 .
 Graphite's design is focused on providing simple interfaces (both to
 users and applications), real-time visualization, high-availability,
 and enterprise scalability.
